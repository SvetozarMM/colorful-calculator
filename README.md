## Color-Calculator

A simple calculator with basic functionalities. It is possible to dynamically change the colors of the buttons, body, screens, etc. Or use already made themes.

### Technologies:
- ##### JavaScript, DOM, HTML, CSS, Sass

### Open it at: http://www.zarmartinov.com/CalculatorProject/index.html
