const calculator = {
    displayValueBig: '0',
    displayValueSmall: '',
    firstOperand: null,
    waitingForSecondOperand: false,
    operator: null,
};

function inputDigit(digit) {
    const { displayValueBig, waitingForSecondOperand } = calculator;

    if (waitingForSecondOperand === true) {
        calculator.displayValueBig = digit;
        calculator.waitingForSecondOperand = false;
    } else {
        calculator.displayValueBig = displayValueBig === '0' ? digit : displayValueBig + digit;
    }
}

function inputDecimal(dot) {
    if (calculator.waitingForSecondOperand === true) {
        return;
    }
    if (!calculator.displayValueBig.includes(dot)) {
        calculator.displayValueBig += dot;
    }
}

function inputNegative() {
    
    if (!calculator.displayValueBig.startsWith("-") && calculator.displayValueBig != '0') {
        calculator.displayValueBig = "-" + calculator.displayValueBig;
    } else if (calculator.displayValueBig.startsWith("-")) {
        calculator.displayValueBig = calculator.displayValueBig.replace(/[-]/, '');  
    }
    if(calculator.firstOperand != null) {
        calculator.firstOperand = calculator.firstOperand * (-1);
    }
}

function handleOperator(nextOperator) {
    const { firstOperand, displayValueBig, operator } = calculator
    const inputValue = parseFloat(displayValueBig);
  
    if (operator && calculator.waitingForSecondOperand)  {
        calculator.operator = nextOperator;
        return;
    }
    if (firstOperand == null) {
        calculator.firstOperand = inputValue;
    } else if (operator) {
        const currentValue = firstOperand || 0;
        const result = performCalculation[operator](currentValue, inputValue);

        calculator.displayValueBig = String(result);
        calculator.firstOperand = result;
        calculator.displayValueSmall = '= ' + result;
    }
    calculator.waitingForSecondOperand = true;
    calculator.operator = nextOperator;
}

const performCalculation = {
    '/': (firstOperand, secondOperand) => firstOperand / secondOperand,
  
    '*': (firstOperand, secondOperand) => firstOperand * secondOperand,
  
    '+': (firstOperand, secondOperand) => firstOperand + secondOperand,
  
    '-': (firstOperand, secondOperand) => firstOperand - secondOperand,
  
    '=': (firstOperand, secondOperand) => secondOperand
};

function resetCalculatorAll() {
    calculator.displayValueBig = '0';
    calculator.displayValueSmall = '';
    calculator.firstOperand = null;
    calculator.waitingForSecondOperand = false;
    calculator.operator = null;
}

function resetCalculator() {
    calculator.displayValueBig = '0';
    calculator.firstOperand = null;
    calculator.waitingForSecondOperand = false;
    calculator.operator = null;
}

function updateDisplayBig() {
    const display = document.querySelector('.calculator__screen-box--big');
    display.value = calculator.displayValueBig;
}

function updateDisplaySmall() {
    const display = document.querySelector('.calculator__screen-box--small');
    display.value = calculator.displayValueSmall;
}

updateDisplayBig();
updateDisplaySmall();

const keys = document.querySelector('.calculator__keys');
keys.addEventListener('click', (event) => {
    const target = event.target;
    if (!target.matches('button')) {
        return;
    }
    if (target.classList.contains('operator')) {
        handleOperator(target.value);
        updateDisplayBig();
        updateDisplaySmall();
        return;
    }
    if (target.classList.contains('decimal')) {
        inputDecimal(target.value);
        updateDisplayBig();
        return;
    }
    if (target.classList.contains('negative')) {
        inputNegative(target.value);
        updateDisplayBig();
        return;
    }
    if (target.classList.contains('all-clear')) {
        resetCalculatorAll();
        updateDisplayBig();
        updateDisplaySmall();
        return;
    }
    if (target.classList.contains('clear')) {
        resetCalculator();
        updateDisplayBig();
        return;
    }
    
    inputDigit(target.value);
    updateDisplayBig();
    updateDisplaySmall();
});

// menu__navi(height) = menu__bgr(height) (Dinamic change)

let bgr = document.getElementById('menu__bgr');
let nav = document.getElementById('menu__nav');
const styleBgr = getComputedStyle(bgr);
const bgrHeight = styleBgr.height;
const styleNav = getComputedStyle(nav);
const navHeight = styleNav.height;

function openBgr() {  
    bgr.style.height = navHeight; //parseInt(navHeight) + 50;
}
function closeBgr() { 
    bgr.style.height = bgrHeight;
}




// -------- Color Picker --------

console.clear();
function $(selector) { 
    return document.querySelector(selector); 
}

//bgr
const calculatorBgr = $('#calculator');
const mainScr = $('#main-screen');
const resultScr = $('#result-screen');
// buttons
const btnPlus = $('.btn--plus');
const btnMinus = $('.btn--minus');
const btnMultipl = $('.btn--multipl');
const btnDivision = $('.btn--division');
const btn1 = $('.btn--1');
const btn2 = $('.btn--2');
const btn3 = $('.btn--3');
const btn4 = $('.btn--4');
const btn5 = $('.btn--5');
const btn6 = $('.btn--6');
const btn7 = $('.btn--7');
const btn8 = $('.btn--8');
const btn9 = $('.btn--9');
const btn0 = $('.btn--0');
const btnAC = $('.btn--ac');
const btnC = $('.btn--c');
const btnEqual = $('.btn--equal');
const btnNeg = $('.btn--neg');
const btnDot = $('.btn--dot');
//fixed parent container
const parentFixed = $('#color-picker');

//Radio Btns
//Bgr
const radioCalculatorBgr = $('#radio-bgr-calc');
const radioMainScr = $('#radio-bgr-main-scr');
const radioResultScr = $('#radio-bgr-result-scr');
const radioButtonsBgr = $('#radio-bgr-btn');
//Numbers
const radioMainNum = $('#radio-num-main-scr');
const radioResultNum = $('#radio-num-result-scr');
const radioButtonsNum = $('#radio-num-btn');

const btnsAllBgr = function(col) {
    btnPlus.style.backgroundColor = col;
    btnMinus.style.backgroundColor = col;
    btnMultipl.style.backgroundColor = col;
    btnDivision.style.backgroundColor = col;
    btn1.style.backgroundColor = col;
    btn2.style.backgroundColor = col;
    btn3.style.backgroundColor = col;
    btn4.style.backgroundColor = col;
    btn5.style.backgroundColor = col;
    btn6.style.backgroundColor = col;
    btn7.style.backgroundColor = col;
    btn8.style.backgroundColor = col;
    btn9.style.backgroundColor = col;
    btn0.style.backgroundColor = col;
    btnAC.style.backgroundColor = col;
    btnC.style.backgroundColor = col;
    btnEqual.style.backgroundColor = col;
    btnNeg.style.backgroundColor = col;
    btnDot.style.backgroundColor = col;
}
const btnsAllNum = function(col) {
    btnPlus.style.color = col;
    btnMinus.style.color = col;
    btnMultipl.style.color = col;
    btnDivision.style.color = col;
    btn1.style.color = col;
    btn2.style.color = col;
    btn3.style.color = col;
    btn4.style.color = col;
    btn5.style.color = col;
    btn6.style.color = col;
    btn7.style.color = col;
    btn8.style.color = col;
    btn9.style.color = col;
    btn0.style.color = col;
    btnAC.style.color = col;
    btnC.style.color = col;
    btnEqual.style.color = col;
    btnNeg.style.color = col;
    btnDot.style.color = col;
}

function getDefaultBgr (element) {
    const tempBgr = getComputedStyle(element);
    return tempBgr.backgroundColor;
}
function getDefaultColor (element) {
    const tempColor = getComputedStyle(element);
    return tempColor.color;
}

//Default picker
const pickerFixed = new Picker({
    parent: parentFixed,
    popup: false,
    alpha: true,
    editor: true,
}); 
pickerFixed.setColor('yellow', true);

//
function isCheked() {
    if(radioCalculatorBgr.checked) {
        calcBgr.show();
        mainBgr.hide();         
        resultBgr.hide();
        pickerFixed.hide();
        buttonsBgr.hide();
        mainNum.hide(); 
        resultNum.hide();
        buttonsNum.hide();

    } else if(radioMainScr.checked) {
        calcBgr.hide();
        mainBgr.show();
        resultBgr.hide();
        pickerFixed.hide();
        buttonsBgr.hide();
        mainNum.hide(); 
        resultNum.hide();
        buttonsNum.hide();

    } else if(radioResultScr.checked) {
        calcBgr.hide();
        mainBgr.hide();
        resultBgr.show();
        pickerFixed.hide();
        buttonsBgr.hide();
        mainNum.hide(); 
        resultNum.hide();
        buttonsNum.hide();

    } else if(radioButtonsBgr.checked) {
        buttonsBgr.show();
        calcBgr.hide();
        mainBgr.hide();
        resultBgr.hide();
        pickerFixed.hide();
        mainNum.hide(); 
        resultNum.hide();
        buttonsNum.hide();

    } else if(radioMainNum.checked) {
        buttonsBgr.hide();
        calcBgr.hide();
        mainBgr.hide();
        resultBgr.hide();
        pickerFixed.hide();
        mainNum.show(); 
        resultNum.hide();
        buttonsNum.hide();

    } else if(radioResultNum.checked) {
        buttonsBgr.hide();
        calcBgr.hide();
        mainBgr.hide();
        resultBgr.hide();
        pickerFixed.hide();
        mainNum.hide(); 
        resultNum.show();
        buttonsNum.hide();

    } else if(radioButtonsNum.checked) {
        buttonsBgr.hide();
        calcBgr.hide();
        mainBgr.hide();
        resultBgr.hide();
        pickerFixed.hide();
        mainNum.hide(); 
        resultNum.hide();
        buttonsNum.show();
    }
    
}

// Picker - objects

//calcBgr
const defBgrCalc = getDefaultBgr(calculatorBgr);
const calcBgr = new Picker({
    parent: parentFixed,
    popup: false,
    alpha: true,
    editor: true,
    color: defBgrCalc,
    onChange: function(color) {
        calculatorBgr.style.backgroundColor = color.rgbaString;
    },
}); 
calcBgr.setColor('yellow', true);
calcBgr.hide();

//mianBgr
const defBgrMain = getDefaultBgr(mainScr);
const mainBgr = new Picker({
    parent: parentFixed,
    popup: false,
    alpha: true,
    editor: true,
    color: defBgrMain,
    onChange: function(color) {
        mainScr.style.backgroundColor = color.rgbaString;
    },
}); 
mainBgr.setColor('yellow', true);
mainBgr.hide();
//mianNum
const defColorMain = getDefaultColor(mainScr);
const mainNum = new Picker({
    parent: parentFixed,
    popup: false,
    alpha: true,
    editor: true,
    color: defColorMain,
    onChange: function(color) {
        mainScr.style.color = color.rgbaString;
    },
}); 
mainNum.setColor('yellow', true);
mainNum.hide();

//resultBgr
const defBgrResult = getDefaultBgr(resultScr);
const resultBgr = new Picker({
    parent: parentFixed,
    popup: false,
    alpha: true,
    editor: true,
    color: defBgrResult,
    onChange: function(color) {
        resultScr.style.backgroundColor = color.rgbaString;
    },
}); 
resultBgr.setColor('yellow', true);
resultBgr.hide();
//resultNum
const defNumResult = getDefaultColor(resultScr);
const resultNum= new Picker({
    parent: parentFixed,
    popup: false,
    alpha: true,
    editor: true,
    color: defNumResult,
    onChange: function(color) {
        resultScr.style.color = color.rgbaString;
    },
}); 
resultNum.setColor('yellow', true);
resultNum.hide();

//buttonsBgr
const defBgrBtns = getDefaultBgr(btnPlus);
const buttonsBgr = new Picker({
    parent: parentFixed,
    popup: false,
    alpha: true,
    editor: true,
    color: defBgrBtns,
    onChange: function(color) {
        btnsAllBgr(color.rgbaString);
    },
}); 
buttonsBgr.setColor('yellow', true);
buttonsBgr.hide();
//buttonsNum
const defNumBtns = getDefaultColor(btnPlus);
const buttonsNum = new Picker({
    parent: parentFixed,
    popup: false,
    alpha: true,
    editor: true,
    color: defNumBtns,
    onChange: function(color) {
        btnsAllNum(color.rgbaString);
    },
}); 
buttonsNum.setColor('yellow', true);
buttonsNum.hide();



// THEMES 


const radioAllGray = $('#radioAllGray');
const radioSnow = $('#radioSnow');
const radioCharry = $('#radioCharry');
const radioSunrise = $('#radioSunrise');
const radioBlueLaguna = $('#radioBlueLaguna');
const radioBlackAndYellow = $('#radioBlackAndYellow');
const radioFrog = $('#radioFrog');
const radioBarbie = $('#radioBarbie');
const radioCandy = $('#radioCandy');


//to initialize default design 
themes();

function themes() {
    if(radioAllGray.checked) {

        calculatorBgr.style.backgroundColor = 'var(--color-gray-darken)';

        mainScr.style.backgroundColor = 'var(--color-gray-darken)';
        mainScr.style.color = 'var(--color-gray-light)';

        resultScr.style.backgroundColor = 'var(--color-gray-darken)';
        resultScr.style.color = 'var(--color-yellow-light)';

        btnsAllBgr('var(--color-gray-darken)');
        btnsAllNum('var(--color-gray-light)');
    }
    if(radioSnow.checked) {

        calculatorBgr.style.backgroundColor = 'white';

        mainScr.style.backgroundColor = 'white';
        mainScr.style.color = 'var(--color-gray-darken)';

        resultScr.style.backgroundColor = 'white';
        resultScr.style.color = 'var(--color-gray-darken)';

        btnsAllBgr('white');
        btnsAllNum('var(--color-gray-darken)');
    }
    if(radioCherry.checked) {

        calculatorBgr.style.backgroundColor = 'var(--color-red-light)';

        mainScr.style.backgroundColor = 'white';
        mainScr.style.color = 'var(--color-gray-darken)';

        resultScr.style.backgroundColor = 'white';
        resultScr.style.color = 'var(--color-red-dark)';

        btnsAllBgr('var(--color-red-dark)');
        btnsAllNum('white');
    }
    if(radioSunrise.checked) {

        calculatorBgr.style.backgroundColor = 'var(--color-yellow-light)';

        mainScr.style.backgroundColor = 'var(--color-yellow-light)';
        mainScr.style.color = 'var(--color-gray-darken)';

        resultScr.style.backgroundColor = 'white';
        resultScr.style.color = 'var(--color-red-dark)';

        btnsAllBgr('var(--color-yellow-light)');
        btnsAllNum('var(--color-gray-darken)');
    }
    if(radioBlueLaguna.checked) {

        calculatorBgr.style.backgroundColor = 'var(--color-blue-dark)';

        mainScr.style.backgroundColor = '#a1c7e4ff';
        mainScr.style.color = 'var(--color-gray-darken)';

        resultScr.style.backgroundColor = '#a1c7e4ff';
        resultScr.style.color = 'white';

        btnsAllBgr('var(--color-blue-dark)');
        btnsAllNum('white');
    }
    if(radioBlackAndYellow.checked) {

        calculatorBgr.style.backgroundColor = 'var(--color-yellow-light)';

        mainScr.style.backgroundColor = 'var(--color-gray-darken)';
        mainScr.style.color = 'var(--color-gray-light)';

        resultScr.style.backgroundColor = 'var(--color-yellow-light)';
        resultScr.style.color = 'var(--color-gray-darken)';

        btnsAllBgr('var(--color-gray-darken)');
        btnsAllNum('var(--color-gray-light)');
    }
    if(radioFrog.checked) {

        calculatorBgr.style.backgroundColor = '#0b9556ff';

        mainScr.style.backgroundColor = '#0b9556ff';
        mainScr.style.color = '#f9f06aff';

        resultScr.style.backgroundColor = '#0b9556ff';
        resultScr.style.color = '#611434ff';

        btnsAllBgr('#0b9556ff');
        btnsAllNum('#f9f06aff');
    }
    if(radioBarbie.checked) {

        calculatorBgr.style.backgroundColor = '#f7225fff';

        mainScr.style.backgroundColor = '#f7225fff';
        mainScr.style.color = 'white';

        resultScr.style.backgroundColor = 'white';
        resultScr.style.color = 'var(--color-gray-darken)';

        btnsAllBgr('#f7225fff');
        btnsAllNum('var(--color-gray-darken)');
    }
    if(radioCandy.checked) {

        calculatorBgr.style.backgroundColor = '#fefa3eff';

        mainScr.style.backgroundColor = '#00d2ffff';
        mainScr.style.color = '#87098eff';

        resultScr.style.backgroundColor = '#fefa3eff';
        resultScr.style.color = '#f7225fff';

        btnsAllBgr('#f7225fff');
        btnsAllNum('white');
    }
}


// Switching Background

let switchingBgr = document.getElementById('container');
let switchingManuIconOpen = document.getElementById('menu-icon--open');
let switchingManuIconClose = document.getElementById('menu-icon--close');
let switchingManuArrowBgr = document.getElementById('icon-arrow-background');
let switchingManuArrowNum = document.getElementById('icon-arrow-numbers');

function switchingBgrBlack() {
    switchingBgr.style.backgroundColor = 'var(--color-gray-darken)';  
    bgr.style.backgroundColor = 'white'; 
    nav.style.color = 'var(--color-gray-darken)';   
    switchingManuIconOpen.style.fill = 'var(--color-gray-darken)'; 
    switchingManuIconClose.style.fill = 'var(--color-gray-darken)'; 
    switchingManuArrowBgr.style.fill = 'var(--color-gray-darken)'; 
    switchingManuArrowNum.style.fill = 'var(--color-gray-darken)'; 
}
function switchingBgrWhite() {
    switchingBgr.style.backgroundColor = 'white';  
    bgr.style.backgroundColor = 'var(--color-gray-darken)'; 
    nav.style.color = 'white';  
    switchingManuIconOpen.style.fill = 'white';
    switchingManuIconClose.style.fill = 'white';
    switchingManuArrowBgr.style.fill = 'white'; 
    switchingManuArrowNum.style.fill = 'white'; 
}

